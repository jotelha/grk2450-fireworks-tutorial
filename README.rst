Scientific workflow is an important technique used in many simulation and data 
analysis applications. In particular, workflows automate high-throughput / 
high-complexity computing applications, enable code and data reuse and 
provenance, provide methods for validation and error tracking, and exploit 
application concurrency using distributed computing resources. The goal of this 
tutorial is to learn composing and running workflow applications using the 
FireWorks workflow environment (https://materialsproject.github.io/fireworks/).
After an introduction to the FireWorks, the participants will learn to construct
workflows using a library of existing Firetasks. The composed workflows will
be verified, visualized and then executed and monitored. The exercises include
managing data and control flow dynamically using FWAction. The goal of Exercise
5 is to learn writing custom Firetasks to match more specific application
requirements. 

Basic knowledge of using the bash shell is required. For Exercise 5 basic 
knowledge of Python is required.
