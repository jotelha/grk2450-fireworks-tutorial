# FireWorks, FilePad and metadata

## Introduction

The Jupyter notebook `PythonGoldCluster.ipynb` contains two
[FireWorks](https://materialsproject.github.io/fireworks/) sample usage
cases. They demonstrate meaningful use of metadata for management of workflow
results within FireWork's *FilePad*.

## Content

* `PythonGoldCluster.ipynb`: a Jupyter notebook working through this demo
  from setting up workflows to anaylizing results, i.e.  from beginning to end
* `A_*.yaml` to `D_*.yaml`: Static yaml descriptions of FireWorks used within
  the first sample case.
* `infiles/*`: static LAMMPS input scripts and data files used in both sample
  cases. The third-party EAM potential file `Au-Grochola-JCP05.eam.alloy` by

    *G. Grochola, S.P. Russo, and I.K. Snook (2005), "On fitting a gold embedded
    atom method potential using the force matching method", The Journal of
    Chemical Physics, 123(20), 204719. DOI: 10.1063/1.2124667.*

  has been excluded from this repository. You will find it publically available
  for download at https://www.ctcms.nist.gov/potentials/system/Au
  (see `infiles/NOTE`)
* `fig/*`: figures and animations used for documentation purposes
* `doc/PythonGoldCluster.html`: a static HTML-rendered version of the Jupyter
  notebook. Does not include any interactive trajectory visualizations via
  `nglview`.
* `doc/FilePad_slides.pdf`: static PDF-rendered slides outlining this
  demonstration's core concepts.
* `doc/gold_droplet_impact.mp4`: An accompanying gold droplet impact animation
  not embedded within the static PDF slides.

## Requirements

The Jupyter notebook has been tested with FireWorks version 1.9.5.
Next to FireWorks, the Python packages
* ase
* matplotlib
* nglview (indirectly via ASE)
* numpy
* pandas
* scipy
are used to facilitate demonstrative analysis of results.

The samples have been designed to run on a custom OpenStack instance. An image
of this instance is available on request from the author (Johannes Hörmann,
johannes.hoermann@imtek.uni-freiburg.de) via the OpenSzack service *bwCloud*
(https://bwcloud.ruf.uni-freiburg.de). Without modifications, these samples will
not run within the framework of this *grk2450-fireworks-tutorial*. Most
importantly, every FirwWork's "_category" spec must point to a worker that
offers a LAMMPS executable. Likewise, *ScriptTask* calls of LAMMPS must
match this particular worker's setup.

The LAMMPS versio used for this demonstration is

    LAMMPS (22 Aug 2018)

and the executable has been built with

    Active compile time flags:

    -DLAMMPS_GZIP
    -DLAMMPS_PNG
    -DLAMMPS_JPEG
    -DLAMMPS_FFMPEG

    Installed packages:

    COLLOID COMPRESS KOKKOS KSPACE MANYBODY MC MOLECULE MPIIO OPT PYTHON REPLICA
    RIGID VORONOI USER-COLVARS USER-DIFFRACTION USER-MOLFILE USER-NETCDF USER-OMP


## Contributors:

* Johannes Hörmann (IMTEK, University of Freiburg, johannes.hoermann@imtek.uni-freiburg.de)
* Prof. Lars Pastewka (IMTEK, University of Freiburg, lars.pastewka@imtek.uni-freiburg.de)
