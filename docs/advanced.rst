Advanced and productive use
===========================


Use firework categories and fireworker names
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If fireworks have to be launched on different workers (for example in
heterogeneous and distributed computing workflows) the keywords ``_category``
and ``_fworker`` can be used to specify which categories of fireworks can be
launched by what fireworkers. In the following example, the firework can be 
launched by any fireworker with name *uc1* that is configured to process category
*turbomole*::

  _category: turbomole
  _fworker: uc1

A matching fireworker is configured in file
**demos/6_advanced/config/fworker_uc1_turbomole.yaml**::

  name: uc1
  category: [turbomole]
  query: '{}'

In this example, the fireworker may launch only fireworks from category *turbomole*
and tagged with fireworker name *uc1*. Note that the same fireworker may process
more than one category or non-categorised fireworks if configured correspondingly.


Submit jobs to the batch system using ``qlaunch``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If a firework launch takes large resources then it should be submitted to a
batch system on the HPC cluster. This is done with the ``qlaunch`` command. The
command is configured with a qadapter configuration file. For configuration files
for the queing systems MOAB and SLURM see the examples provided in the folder
**demos/6_advanced/config**.

In practical use cases, it is generally of advantage to separate the configuration
files, the workflow templates (i.e. the input) and the launches. This structure
is used in folder **demos/6_advanced**.

To submit a single fireworker batch job on bwUniCluster the ``qlaunch`` command
is called like this in the **launches** folder::

    cd launches
    qlaunch -q ../config/qadapter_uc1_vasp.yaml -w ../config/fworker_uc1_vasp.yaml singleshot

The submitted batch jobs can be managed with the commands of your batch system.
For example, for SLURM these are the ``squeue`` and the ``scontrol`` commands.
The sample adapters are configured in such a way that all launch directories will
be created there where the ``qlaunch`` command was executed. For large outputs
and large temporary data the best place of **launches** and **config** folders
is within a workspace of a high-throughput cluster file system.



Deal with failures and crashes
------------------------------

For some reason the execution of a firework may fail and the firework gets 
*FIZZLED* state. Depending on the reason for the error there are different
approaches to handle the error:

* If the error is external (i.e. not in the ``spec``) and fixed then the firework
  can be rerun using the command ``lpad rerun_fws``.
* If the the error is in the *spec* of the firework then this can be in-place
  fixed with the command ``lpad update_fws`` and then rerun with ``lpad rerun_fws``.
  Advantage: preservation of all independent *COMPLETED* fireworks is guaranteed.
* If the error is in the *spec* then it can be fixed in the workflow template
  and the whole workflow is added again to launchpad. This approach is not
  practical with increasing number of errors and updates in the same workflow.

Detect lost runs
----------------

If a job is killed by the batch system its status *RUNNING* gets never changed.
In order to detect such running fireworks we use the command ``lpad detect_lostruns``
which will return the IDs of fireworks with lost runs. Optionally, these can be
rerun or set to *FIZZLED*.


Detect duplicates
-----------------

Fireworks can reuse the data from the launches of identical Fireworks (duplicates).
To enable detection of duplicates the following key is added to the *spec*::

  _dupefinder:
    _fw_name: DupeFinderExact

Two or more identical Fireworks that have this key will be automatically
detected by ``rlaunch`` or by ``qlaunch`` on the Fireworker. After that the
state of the more recent firework is changed from *READY* to *COMPLETED* without
launching it and begins sharing the (most recent) launcher of the duplicate(s).
To understand how this works let us start two workflows **dupefinder_1.yaml**
and **dupefinder_1.yaml** in **demos/6_advanced/inputs/** have a whole
identical section including four fireworks (1-4)::

  lpad reset
  lpad add -c dupefinder_1.yaml
  rlaunch rapidfire
  lpad add -c dupefinder_2.yaml
  rlaunch rapidfire

The second run of ``rlaunch`` detects four duplicate pairs whereas only the last
firework of the second added workflow is executed. After this both workflows are
in *COMPLETED* state which can be checked with::

  lpad get_wflows -t -m 2 --rsort created_on

Let us now delete the first workflow for that all fireworks have been executed::

  lpad delete_wflows -i 1
  
We see *Remove launches []* in the output, i.e. its launches have not been
removed. With deleting a workflow including duplicated fireworks the shared
launcher is removed from launchpad only if all duplicated fireworks are deleted.
The launches are related now only to the relevant fireworks of the second
workflow. The launches will be removed if we remove the second workflow::

  lpad delete_wflows -i 6

We see in the output the message *Remove launches [1, 3, 3, 4, 5]*.

Not only the launches but also the state of identical fireworks is shared. Let
us test this with re-running a firework that is identical to another firework::

  lpad add -c dupefinder_2.yaml
  lpad add -c dupefinder_1.yaml
  rlaunch rapidfire
  # only first five executed, the last four marked as duplicates
  lpad get_wflows -t -m 2 --rsort created_on
  # all two COMPLETED
  lpad rerun_fws -i 1
  lpad get_wflows -t -m 2 --rsort created_on
  # the fireworks with ids 1 and 6 are in READY state, all the rest WAITING
  rlaunch rapidfire
  # again five fireworks are executed (why not only the first?)
  # and four duplicate pairs detected
  lpad get_wflows -t -m 2 --rsort created_on
  # the two fireworks are COMPLETED


Query and analyse data from fireworks and workflows
---------------------------------------------------

Fireworks and workflows are stored on the LaunchPad during their full life cycle
-- at the time they are added and become in *READY* state, as they are run in
*RUNNING* state until they reach the *COMPLETED* state. Completed workflows
hold not only the input parameters and the results but also provenence metadata
that help the further use of the workflows. The life cycle of a workflow continues
when it is extended using for example the *append_wflow* command (see exercise 4).

Another further use of the stored workflows is to collect, reorganize, analyse
and visualize their stored data programatically. To show how data can be extracted
from workflows and fireworks a very simple query module is demonstrated in
**lib/lpad_query.py**. The module is driven by a command ``lpad_query``
(installed in **bin**) and has similar syntax as the ``lpad`` command. For a
short help this command can be started like::

  lpad_query --help
  
In **demos/6_advanced/analysis** three sample queries are prepared. The first 
query::

  filters:
    name: The coffee workflow
    state: COMPLETED
  selects: []

will return all completed workflows that have name *The coffee workflow*. The
empty selection means that no firework and no firework updates are selected.
This query is started with the command:: 

  lpad_-o yaml query -f query_sample_1.yaml

If, supposed, we have one workflow with that name and it is completed the output
is::

  - fws: []
    metadata: {}
    name: The coffee workflow

Because no fireworks or updates are selected the fws list is empty. The second
query selects for each returned document the fireworks with name *Brew coffee*::

  selects:
  - fw_name: Brew coffee

The query returns again a list with one workflow (because the filter is the same)
but this time with one firework (metadata and updates)::

  - fws:
    - created_on: '2019-12-05T13:42:42.535429'
      id: 222
      name: Brew coffee
      parents:
      - 221
      state: COMPLETED
      updated_on: '2019-12-05T13:42:52.854682'
      updates:
        pure coffee:
        - top coffee selection
        - workflowing water
    metadata: {}
    name: The coffee workflow

If we now add the key ``add fw_spec`` to the selects::

  selects:
  - fw_name: Brew coffee
    add fw_spec: true

the returned data is completed with the specs of the selected fireworks::

  - fws:
    - created_on: '2019-12-05T13:42:42.535429'
      id: 222
      name: Brew coffee
      parents:
      - 221
      spec:
        _tasks:
        - _fw_name: PyTask
          func: auxiliary.print_func
          inputs:
          - coffee powder
          - water
          outputs:
          - pure coffee
        coffee powder: top coffee selection
        water: workflowing water
      state: COMPLETED
      updated_on: '2019-12-05T13:42:52.854682'
      updates:
        pure coffee:
        - top coffee selection
        - workflowing water
    metadata: {}
    name: The coffee workflow

The third example demonstrates even more complex use of a query for a DFT
calculation of a water molecule. The query includes a regular expression and the
workflow metadata in the filter section. Additionally, more than one firework is
selected and for one firework specific updates (from all updates) are selected.

For a detailed tutorial on queries it is referred to the MongoDB query tutorial
(select there your programming language): 

https://docs.mongodb.com/manual/tutorial/query-documents/

In the examples provided in this tutorial the queries are serialized to YAML and
to JSON.


Final remark
------------

Not every feature of FireWorks is covered in this tutorial. Please visit the
the documentation website https://materialsproject.github.io/fireworks/ for
detailed tutorials and comprehensive documentation.
