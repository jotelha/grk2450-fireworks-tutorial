Setup
=====

* Install Python
* Install and configure FireWorks
* Download and setup the tutorial
* Install additional packages

Basic procedures
================

* Compose workflows
* Validate workflows
* Add workflows to LaunchPad
* Visualize workflows
* Launch fireworks
* Query fireworks and workflows
* Rerun fireworks
* Remove workflows

Exercise 1: Managing control flow 
=================================

* Dependencies and concurrency
* Use built-in Firetasks: ``ScriptTask``
* Example: F1 pitstop

Exercise 2: Managing data flow
==============================

* Data flow dependencies 
* Use ``PyTask``
* Example: Recruiting

Exercise 3: Manage data in files and command line input
=======================================================

* Use ``CommandLineTask`` and ``_files_in`` and ``_files_out`` keys
* Use ``CommandLineTask``
* Example: Image reconstruction

Exercise 4: Extending a workflow
================================

* Use command ``lpad append_wflow``
* Extension of example from Exercise 3
* Example: Image swirl

Exercise 5: Writing a Firetask
==============================

* Extension of example from Exercise 2

Advanced and productive use
===========================

* Using FireWorks with a batch system on HPC clusters
* Separate configuration, launches and templates
* Use keywords _category and _fworker
* Recover from failures
* Detect duplicates
* Detect lost runs
* Use FilePad to store files
* Query and analyse data from fireworks and workflows