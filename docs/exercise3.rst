Exercise 3: Manage data in files and command line input
=======================================================

The purpose of this exercise is to learn how to pass data files between Fireworks
and process these data. There are two alternative ways to solve this problem: 1)
using the built-in ``ScriptTask`` in combination with ``_files_in`` and
``_files_out`` spec keys; 2) using the build-in ``CommandLineTask``.

Using ``ScriptTask``, and ``_files_in`` and ``_files_out`` keys
---------------------------------------------------------------

The built-in ``ScriptTask`` used in **Exercise 1** allows to run a shell script
(or an array of scripts) containing a command with static input parameters, such
as command line flags and data files used for input and output. Data files can
be passed from parent fireworks to their children using the ``_files_in`` and
``_files_out`` spec keys. In the following example the file **output.png** is
produced in firework 1 and this file is made available to all children (in
this example firework 2) under the meta-name *top left* where it is copied under
the real file name **input.png**:: 

  fws:
  - fw_id: 1
    name: Rotate 90 degrees anti-clockwise
    spec:
      _tasks:
      - _fw_name: ScriptTask
        script: convert -rotate -90 piece-2.png output.png
      _files_out:
        top left: output.png
  - fw_id: 2
    name: Flop horizontally
    spec:
      _tasks:
      - _fw_name: ScriptTask
        script: convert -flop input.png output.png
      _files_in:
        top left: input.png
      _files_out:
        top right: output.png
  links: {'1': [2]}
  metadata: {}

The output of firework 2, **output.png** is then provided under the meta-name
*top right*. More examples can be found in folder **demos/3_files_and_commands**
with the extension *_scripttask*.

Using the ``CommandLineTask``
-----------------------------

This method is generally more powerful than using the ``ScriptTask`` but also more
complex to use. Many examples can be found in folder **demos/3_files_and_commands**.
A complete documentation is provided on the fireworks website
https://materialsproject.github.io/fireworks/dataflow_tasks.html.


Problems
--------

Given is a set of reusable operations implemented using the ``convert`` and 
``montage`` commands from the *ImageMagick* package with different sets of 
command line flags. These are:

* Rotate +/-90°, 180° 
* Horizontal mirror
* Vertical mirror
* Montage
* Swirl
* Animate

The Fireworks corresponding to these operations can be found in 
**exercises/demos/3_files_and_commands**. The provided solutions in 
**exercises/solutions/3_files_and_commands** are suitable for the input for 
letter "A" in **exercises/inputs/3_files_and_commands/A**. 

Problem 3.1
~~~~~~~~~~~

The input image files are some parts of 2 × 2 tiled capital letters. Some of 
the input tiles are rotated or mirrored vertically or horizontally and some missing 
tiles can be recovered by the same operations using the symmetry. The task is 
to recover all tiles and put them together to reconstruct the image of the 
selected letter.

Select a set of input images (**piece-1.png** and **piece-2.png**) for a letter 
from the folder **exercises/inputs/3_files_and_commands**. 
Then use the provided Fireworks and compose a workflow to reconstruct the 
selected letter by adjusting the image processing parameters, inputs and outputs. 
Verify, add and run the workflow and check the resulting image.


Problem 3.2
~~~~~~~~~~~

Given an image as input, "swirl" the image at the angles 90°, 180°, 270° and 360°
producing four new images. Arrange the original and the resulting images in the 
sequence: 

0° → 90° → 180° → 270° → 360° → 270° → 180° → 90° → 0°

and make an animation. As input you can use either the image file **letter.png**
from **exercises/inputs/3_files_and_commands** or the reconstructed image from
**Problem 3.1**.
