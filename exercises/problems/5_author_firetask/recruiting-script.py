import json
from recruiting import *

with open('parameters.json', 'r') as inp:
    parameters = json.load(inp)

template = parameters['application template']
job = parameters['job description']
max_to_apply = parameters['maximum applications']
min_score = parameters['minimum score']
number_to_invite = parameters['number to invite']
number_to_screen = parameters['number to screen']
number_to_fill = parameters['number to fill']

applied = []
while len(applied) < number_to_screen:
    applied = candidates_apply(template, max_to_apply)

screened = []
while len(screened) < number_to_invite:
    screened = screen_candidates(job, applied, min_score, number_to_invite)

selected = interview_candidates(job, screened, number_to_fill)
print(selected)
