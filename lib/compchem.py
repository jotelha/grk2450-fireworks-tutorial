""" simple computational chemistry interfaces using ASE """
import numpy as np
from ase import Atoms
from ase.io import read
from ase.calculators.calculator import get_calculator_class

def cleanup_atoms_dict(adict):
    """ delete keywords in dictionary not allowed in Atoms.__init__ """

    dellist = ['unique_id', 'magmom', 'calculator', 'calculator_parameters',
               'energy', 'forces', 'dipole', 'stress', 'free_energy',
               'charges']
    subdict = {'initial_magmoms': 'magmoms'}
    for key in subdict:
        if key in adict:
            adict[subdict[key]] = adict.pop(key)
    for key in dellist:
        if key in adict:
            del adict[key]
    return adict

@classmethod
def from_dict(cls, adict):
    """ deserialize json serializable dictionaries to atoms objects """
    from ase.io import jsonio    
    from ase.constraints import dict2constraint

    adict_ = jsonio.decode(jsonio.encode(cleanup_atoms_dict(adict)))
    if 'constraints' in adict_:
        constr = adict_.pop('constraints')
        if isinstance(constr, list):
            adict_['constraint'] = [dict2constraint(c) for c in constr]
        else:
            adict_['constraint'] = [dict2constraint(constr)]

    return cls(**adict_)

Atoms.from_dict = from_dict

def atoms2dict(atoms):
    """ convert Atoms object to a dictionary used to recreate the object """
    from ase.io import jsonio
    from ase.db.row import atoms2dict
    import json

    adict = cleanup_atoms_dict(atoms2dict(atoms))
    return json.loads(jsonio.encode(adict))

def get_vibrational_energies(calc):
    """ an interface to return vibrational energies in eV """
    if calc.__class__.__name__ == 'Vasp':
        # returns in meV
        (vib_ene_real, vib_ene_imag) = calc.read_vib_freq()
        vib_ene_real = np.array(vib_ene_real)*1.0e-3
        vib_ene_imag = np.array(vib_ene_imag)*1.0e-3
    elif calc.__class__.__name__ == 'Turbomole':
        # returns in cm^-1
        spectrum = calc.get_results()['vibrational spectrum']
        enes_li = [mode['frequency']['value']/8065.54429 for mode in spectrum]
        enes = np.array(enes_li)
        vib_ene_real = enes[enes>0]
        vib_ene_imag = abs(enes[enes<0])
    return vib_ene_real, vib_ene_imag

def generic_calculation(struct_dict, constr_list, calc_dict):
    """ generic calculation using a structure and list of contraints """
    from ase.constraints import dict2constraint

    if struct_dict.get('atoms'):
        atoms = Atoms.from_dict(struct_dict['atoms'])
    elif struct_dict.get('file'):
        atoms = read(struct_dict['file'])
    if constr_list is not None and len(constr_list) > 0:
        constraints = [dict2constraint(c) for c in constr_list]
        atoms.set_constraint(constraints)
    calc_class = get_calculator_class(calc_dict['name'])
    calc = calc_class(**calc_dict['parameters'])
    atoms.set_calculator(calc)
    calc.calculate(atoms)
    assert calc.converged
    return calc, atoms

def calculate_forces(struct_dict, constr_list, calc_dict):
    """ perform a gradient calculation """
    calc, atoms = generic_calculation(struct_dict, constr_list, calc_dict)
    return atoms.get_forces()

def structure_relaxation(struct_dict, constr_list, calc_dict):
    """ perform a structure optimization with constraints """

    calc, atoms = generic_calculation(struct_dict, constr_list, calc_dict)
    energy = atoms.get_potential_energy()
    forces = atoms.get_forces()
    dipole = atoms.get_dipole_moment()
    struct_dict['atoms'] = atoms2dict(atoms)
    return struct_dict, energy, forces, dipole

def calculate_vibrations(struct_dict, constr_list, calc_dict):
    """ perform normal mode analysis with constraints """

    calc, atoms = generic_calculation(struct_dict, constr_list, calc_dict)
    (vib_ene_real, vib_ene_imag) = get_vibrational_energies(calc)
    zpe = 0.5*np.sum(vib_ene_real)
    tst = len(vib_ene_imag) == 1
    minimum = len(vib_ene_imag) == 0
    return vib_ene_real, zpe, tst, minimum
